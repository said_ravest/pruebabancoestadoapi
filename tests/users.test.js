const { getUsers } = require('../controllers/users')
const { createConecction }  = require('../database/database');
const supertest = require('supertest')
const app = require('../app')

let testServer;
beforeAll(() => {
    createConecction()
    testServer = app.listen(4000)
})

const api = supertest(app)

describe("GET /api/usuarios", () => {
    test("Users are returned as json" , async ()  => {
        await api
        .get('/api/usuarios')
        .expect(200)
        .expect('Content-Type', /application\/json/)
    })  
    test("There are one user", async () => {
        const response = await api.get('/api/usuarios')
        expect(response.body.data).toHaveLength(1)
    })
})

afterAll((done) => {
    testServer.close(done)
})